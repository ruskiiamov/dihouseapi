<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Exception;

use LaptopDev\DihouseApi\Contract\Request;
use Psr\Http\Message\ResponseInterface;

class ResponseException extends \Exception
{
    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * @var Request
     */
    private $request;

    public function __construct(ResponseInterface $response, Request $request)
    {
        $requestNameArr = explode('\\', get_class($request));
        $requestName = array_pop($requestNameArr);

        parent::__construct('DihouseApi Response error: ' . $requestName .  ' - Response body is not XML');

        $this->response = $response;
        $this->request = $request;
    }

    /**
     * @return ResponseInterface
     */
    public function response(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * @return Request
     */
    public function request(): Request
    {
        return $this->request;
    }
}