<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Exception;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use LaptopDev\DihouseApi\Contract\Request;

class RequestException extends Exception
{
    /** @var GuzzleException */
    private $guzzleException;

    /** @var Request */
    private $request;

    public function __construct(GuzzleException $guzzleException, Request $request)
    {
        $requestNameArr = explode('\\', get_class($request));
        $requestName = array_pop($requestNameArr);

        parent::__construct(
            'DihouseApi Request error: ' . $requestName . ' - ' . $guzzleException->getMessage(),
            $guzzleException->getCode()
        );

        $this->guzzleException = $guzzleException;
        $this->request = $request;
    }

    /**
     * @return GuzzleException
     */
    public function guzzleException(): GuzzleException
    {
        return $this->guzzleException;
    }

    /**
     * @return Request
     */
    public function request(): Request
    {
        return $this->request;
    }
}