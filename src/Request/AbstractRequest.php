<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request;

use LaptopDev\DihouseApi\Contract\Request;
use LaptopDev\DihouseApi\Contract\Response;

abstract class AbstractRequest implements Request
{
    const RESPONSE = Response::class;

    public function responseClassName(): string
    {
        return static::RESPONSE;
    }
}