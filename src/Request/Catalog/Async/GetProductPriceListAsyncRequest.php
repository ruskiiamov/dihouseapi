<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\Catalog\CatalogRequest;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductPriceListAsyncResponse;

/**
 * @JMS\XmlRoot("urn:GetProductPriceListAsync", namespace="urn:di-house.ru:CEI")
 */
class GetProductPriceListAsyncRequest extends CatalogRequest
{
    const RESPONSE = GetProductPriceListAsyncResponse::class;
}