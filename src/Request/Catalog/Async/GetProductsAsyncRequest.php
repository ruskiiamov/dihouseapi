<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\Catalog\CatalogRequest;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductsAsyncResponse;

/**
 * @JMS\XmlRoot("urn:GetProductsAsync", namespace="urn:di-house.ru:CEI")
 */
class GetProductsAsyncRequest extends CatalogRequest
{
    const RESPONSE = GetProductsAsyncResponse::class;
}