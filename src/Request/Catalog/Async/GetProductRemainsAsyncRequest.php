<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\Catalog\CatalogRequest;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductRemainsAsyncResponse;

/**
 * @JMS\XmlRoot("urn:GetProductRemainsAsync", namespace="urn:di-house.ru:CEI")
 */
class GetProductRemainsAsyncRequest extends CatalogRequest
{
    const RESPONSE = GetProductRemainsAsyncResponse::class;
}