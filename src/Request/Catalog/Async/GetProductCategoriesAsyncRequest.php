<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\AbstractRequest;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductCategoriesAsyncResponse;

/**
 * @JMS\XmlRoot("urn:GetProductCategoriesAsync", namespace="urn:di-house.ru:CEI")
 */
class GetProductCategoriesAsyncRequest extends AbstractRequest
{
    const RESPONSE = GetProductCategoriesAsyncResponse::class;
}