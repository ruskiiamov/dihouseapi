<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog\Sync;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\Catalog\CatalogRequest;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductRemainsSyncResponse;

/**
 * @JMS\XmlRoot("urn:GetProductRemainsSync", namespace="urn:di-house.ru:CEI")
 */
class GetProductRemainsSyncRequest extends CatalogRequest
{
    const RESPONSE = GetProductRemainsSyncResponse::class;
}