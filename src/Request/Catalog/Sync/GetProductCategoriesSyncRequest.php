<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog\Sync;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\AbstractRequest;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductCategoriesSyncResponse;

/**
 * @JMS\XmlRoot("urn:GetProductCategoriesSync", namespace="urn:di-house.ru:CEI")
 */
class GetProductCategoriesSyncRequest extends AbstractRequest
{
    const RESPONSE = GetProductCategoriesSyncResponse::class;
}