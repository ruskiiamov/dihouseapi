<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog\Sync;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\Catalog\CatalogRequest;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductPriceListSyncResponse;

/**
 * @JMS\XmlRoot("urn:GetProductPriceListSync", namespace="urn:di-house.ru:CEI")
 */
class GetProductPriceListSyncRequest extends CatalogRequest
{
    const RESPONSE = GetProductPriceListSyncResponse::class;
}