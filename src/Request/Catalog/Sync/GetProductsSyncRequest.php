<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog\Sync;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\Catalog\CatalogRequest;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductsSyncResponse;

/**
 * @JMS\XmlRoot("urn:GetProductsSync", namespace="urn:di-house.ru:CEI")
 */
class GetProductsSyncRequest extends CatalogRequest
{
    const RESPONSE = GetProductsSyncResponse::class;
}