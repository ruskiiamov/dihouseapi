<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Catalog;

use DateTime;
use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\AbstractRequest;

abstract class CatalogRequest extends AbstractRequest
{
    /**
     * @JMS\SerializedName("urn:Code")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $code;

    /**
     * @JMS\SerializedName("urn:Category")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $category;

    /**
     * @JMS\SerializedName("urn:LastChangeDate")
     * @JMS\Type("DateTime<'d.m.Y H:i:s'>")
     * @JMS\XmlElement(cdata = false)
     *
     * @var DateTime|null
     */
    private $lastChangeDate;

    /**
     * @return string|null
     */
    public function code(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string|null
     */
    public function category(): ?string
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return $this
     */
    public function setCategory(string $category): self
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function lastChangeDate(): ?DateTime
    {
        return $this->lastChangeDate;
    }

    /**
     * @param DateTime $lastChangeDate
     * @return $this
     */
    public function setLastChangeDate(DateTime $lastChangeDate): self
    {
        $this->lastChangeDate = $lastChangeDate;
        return $this;
    }
}