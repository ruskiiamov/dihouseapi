<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Order\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\AbstractRequest;
use LaptopDev\DihouseApi\Response\Order\Async\CancelOrderAsyncResponse;

/**
 * @JMS\XmlRoot("urn:CancelOrderAsync", namespace="urn:di-house.ru:CEI")
 */
class CancelOrderAsyncRequest extends AbstractRequest
{
    const RESPONSE = CancelOrderAsyncResponse::class;

    /**
     * @JMS\SerializedName("urn:OrderRef")
     * @JMS\Type("LaptopDev\DihouseApi\Common\OrderRef")
     * @Required
     *
     * @var OrderRef
     */
    private $orderRef;

    /**
     * @return OrderRef
     */
    public function orderRef(): OrderRef
    {
        return $this->orderRef;
    }

    /**
     * @param OrderRef $orderRef
     * @return $this
     */
    public function setOrderRef(OrderRef $orderRef): self
    {
        $this->orderRef = $orderRef;
        return $this;
    }
}