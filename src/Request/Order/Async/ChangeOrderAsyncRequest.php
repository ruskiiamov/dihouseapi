<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Order\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\OrderEntry;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\AbstractRequest;
use LaptopDev\DihouseApi\Response\Order\Async\ChangeOrderAsyncResponse;

/**
 * @JMS\XmlRoot("urn:ChangeOrderAsync", namespace="urn:di-house.ru:CEI")
 */
class ChangeOrderAsyncRequest extends AbstractRequest
{
    const RESPONSE = ChangeOrderAsyncResponse::class;

    /**
     * @JMS\SerializedName("urn:OrderRef")
     * @JMS\Type("LaptopDev\DihouseApi\Common\OrderRef")
     * @Required
     *
     * @var OrderRef
     */
    private $orderRef;

    /**
     * @JMS\SerializedName("urn:OrderEntriesType")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $orderEntriesType;

    /**
     * @JMS\SerializedName("urn:OrderEntries")
     * @JMS\XmlList(entry = "urn:OrderEntry")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\OrderEntry>")
     * @Required
     *
     * @var OrderEntry[]
     */
    private $orderEntries;

    /**
     * @return OrderRef
     */
    public function orderRef(): OrderRef
    {
        return $this->orderRef;
    }

    /**
     * @param OrderRef $orderRef
     * @return $this
     */
    public function setOrderRef(OrderRef $orderRef): self
    {
        $this->orderRef = $orderRef;
        return $this;
    }

    /**
     * @return int
     */
    public function orderEntriesType(): int
    {
        return $this->orderEntriesType;
    }

    /**
     * @param $orderEntriesType
     * @return $this
     */
    public function setOrderEntriesType($orderEntriesType): self
    {
        $this->orderEntriesType = $orderEntriesType;
        return $this;
    }

    /**
     * @return OrderEntry[]
     */
    public function orderEntries(): array
    {
        return $this->orderEntries;
    }

    /**
     * @param OrderEntry[] $orderEntries
     * @return $this
     */
    public function setOrderEntries(array $orderEntries): self
    {
        $this->orderEntries = $orderEntries;
        return $this;
    }

    /**
     * @param OrderEntry $orderEntry
     * @return $this
     */
    public function addOrderEntry(OrderEntry $orderEntry): self
    {
        $this->orderEntries[] = $orderEntry;
        return $this;
    }
}