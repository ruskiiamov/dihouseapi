<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Order\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\ExtraInfoItem;
use LaptopDev\DihouseApi\Common\OrderEntry;
use LaptopDev\DihouseApi\Request\AbstractRequest;
use LaptopDev\DihouseApi\Response\Order\Async\NewOrderAsyncResponse;

/**
 * @JMS\XmlRoot("urn:NewOrderAsync", namespace="urn:di-house.ru:CEI")
 */
class NewOrderAsyncRequest extends AbstractRequest
{
    const RESPONSE = NewOrderAsyncResponse::class;

    /**
     * @JMS\SerializedName("urn:OrderType")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $orderType;

    /**
     * @JMS\SerializedName("urn:OrderNumber")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     *
     * @var string|null
     */
    private $orderNumber;

    /**
     * @JMS\SerializedName("urn:OrderEntries")
     * @JMS\XmlList(entry = "urn:OrderEntry")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\OrderEntry>")
     * @Required
     *
     * @var OrderEntry[]
     */
    private $orderEntries;

    /**
     * @JMS\SerializedName("urn:ExtraInfo")
     * @JMS\XmlList(entry = "urn:ExtraInfoItem")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ExtraInfoItem>")
     *
     * @var ExtraInfoItem[]|null
     */
    private $extraInfo;

    /**
     * @return int
     */
    public function orderType(): int
    {
        return $this->orderType;
    }

    /**
     * @param int $orderType
     * @return $this
     */
    public function setOrderType(int $orderType): self
    {
        $this->orderType = $orderType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function orderNumber(): ?string
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return $this
     */
    public function setOrderNumber(string $orderNumber): self
    {
        $this->orderNumber = $orderNumber;
        return $this;
    }

    /**
     * @return OrderEntry[]
     */
    public function orderEntries(): array
    {
        return $this->orderEntries;
    }

    /**
     * @param OrderEntry[] $orderEntries
     * @return $this
     */
    public function setOrderEntries(array $orderEntries): self
    {
        $this->orderEntries = $orderEntries;
        return $this;
    }

    /**
     * @param OrderEntry $orderEntry
     * @return $this
     */
    public function addOrderEntry(OrderEntry $orderEntry): self
    {
        $this->orderEntries[] = $orderEntry;
        return $this;
    }

    /**
     * @return ExtraInfoItem[]|null
     */
    public function extraInfo(): ?array
    {
        return $this->extraInfo;
    }

    /**
     * @param ExtraInfoItem[] $extraInfo
     * @return $this
     */
    public function setExtraInfo(array $extraInfo): self
    {
        $this->extraInfo = $extraInfo;
        return $this;
    }

    /**
     * @param ExtraInfoItem $extraInfoItem
     * @return $this
     */
    public function addExtraInfoItem(ExtraInfoItem $extraInfoItem): self
    {
        $this->extraInfo[] = $extraInfoItem;
        return $this;
    }
}