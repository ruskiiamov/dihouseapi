<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Order\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\ExtraInfoItem;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\AbstractRequest;
use LaptopDev\DihouseApi\Response\Order\Async\ReserveToOrderAsyncResponse;

/**
 * @JMS\XmlRoot("urn:ReserveToOrderAsync", namespace="urn:di-house.ru:CEI")
 */
class ReserveToOrderAsyncRequest extends AbstractRequest
{
    const RESPONSE = ReserveToOrderAsyncResponse::class;

    /**
     * @JMS\SerializedName("urn:OrderRef")
     * @JMS\Type("LaptopDev\DihouseApi\Common\OrderRef")
     * @Required
     *
     * @var OrderRef
     */
    private $orderRef;

    /**
     * @JMS\SerializedName("urn:ExtraInfo")
     * @JMS\XmlList(entry = "urn:ExtraInfoItem")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ExtraInfoItem>")
     *
     * @var ExtraInfoItem[]|null
     */
    private $extraInfo;

    /**
     * @return OrderRef
     */
    public function orderRef(): OrderRef
    {
        return $this->orderRef;
    }

    /**
     * @param OrderRef $orderRef
     * @return $this
     */
    public function setOrderRef(OrderRef $orderRef): self
    {
        $this->orderRef = $orderRef;
        return $this;
    }

    /**
     * @return ExtraInfoItem[]|null
     */
    public function extraInfo(): ?array
    {
        return $this->extraInfo;
    }

    /**
     * @param ExtraInfoItem[] $extraInfo
     * @return $this
     */
    public function setExtraInfo(array $extraInfo): self
    {
        $this->extraInfo = $extraInfo;
        return $this;
    }

    /**
     * @param ExtraInfoItem $extraInfoItem
     * @return $this
     */
    public function addExtraInfoItem(ExtraInfoItem $extraInfoItem): self
    {
        $this->extraInfo[] = $extraInfoItem;
        return $this;
    }
}