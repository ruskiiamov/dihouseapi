<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Request\Order;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Request\AbstractRequest;
use LaptopDev\DihouseApi\Response\Order\GetResponseResponse;

/**
 * @JMS\XmlRoot("urn:GetResponse", namespace="urn:di-house.ru:CEI")
 */
class GetResponseRequest extends AbstractRequest
{
    const RESPONSE = GetResponseResponse::class;

    /**
     * @JMS\SerializedName("urn:RequestID")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $requestId;

    /**
     * @return string
     */
    public function requestId(): string
    {
        return $this->requestId;
    }

    /**
     * @param string $requestId
     * @return $this
     */
    public function setRequestId(string $requestId): self
    {
        $this->requestId = $requestId;
        return $this;
    }
}