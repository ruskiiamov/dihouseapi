<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Client;

use GuzzleHttp\Exception\GuzzleException;
use LaptopDev\DihouseApi\Contract\Response;
use LaptopDev\DihouseApi\Request\Order\Async\CancelOrderAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\Async\ChangeOrderAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\Async\NewOrderAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\Async\OrderInfoAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\Async\ReserveToOrderAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\GetResponseRequest;


class Order extends Client
{
    /**
     * @param NewOrderAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendNewOrderAsyncRequest(NewOrderAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param ReserveToOrderAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendReserveToOrderAsyncRequest(ReserveToOrderAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param CancelOrderAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendCancelOrderAsyncRequest(CancelOrderAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param ChangeOrderAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendChangeOrderAsyncRequest(ChangeOrderAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param OrderInfoAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendOrderInfoAsyncRequest(OrderInfoAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetResponseRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetResponseRequest(GetResponseRequest $request): Response
    {
        return $this->sendRequest($request);
    }
}