<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Client;

use GuzzleHttp\Exception\GuzzleException;
use LaptopDev\DihouseApi\Contract\Response;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductCategoriesAsyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductPriceListAsyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductRemainsAsyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductsAsyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductCategoriesSyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductPriceListSyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductRemainsSyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductsSyncRequest;


class Catalog extends Client
{
    /**
     * @param GetProductCategoriesSyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetProductCategoriesSyncRequest(GetProductCategoriesSyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetProductCategoriesAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetProductCategoriesAsyncRequest(GetProductCategoriesAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetProductsSyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetProductsSyncRequest(GetProductsSyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetProductsAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetProductsAsyncRequest(GetProductsAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetProductRemainsSyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetProductRemainsSyncRequest(GetProductRemainsSyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetProductRemainsAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetProductRemainsAsyncRequest(GetProductRemainsAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetProductPriceListSyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetProductPriceListSyncRequest(GetProductPriceListSyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }

    /**
     * @param GetProductPriceListAsyncRequest $request
     * @return Response
     * @throws GuzzleException
     */
    public function sendGetProductPriceListAsyncRequest(GetProductPriceListAsyncRequest $request): Response
    {
        return $this->sendRequest($request);
    }
}