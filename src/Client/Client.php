<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Client;

use DOMDocument;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Contract\Request;
use LaptopDev\DihouseApi\Contract\Response;
use LaptopDev\DihouseApi\Exception\RequestException;
use LaptopDev\DihouseApi\Exception\ResponseException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

abstract class Client
{
    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    /**
     * @param ClientInterface $httpClient
     * @param SerializerInterface $serializer
     * @param string $login
     * @param string $password
     */
    public function __construct(
        ClientInterface $httpClient,
        SerializerInterface $serializer,
        string $login,
        string $password
    ) {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    protected function sendRequest(Request $request): Response
    {
        try {
            $response = $this->httpClient->request(
                'POST',
                '',
                $this->extractOptions($request)
            );
        } catch (GuzzleException $e) {
            throw new RequestException($e, $request);
        }

        return $this->deserialize($request, $response);
    }

    /**
     * @param Request $request
     * @return array
     * @throws Exception
     */
    protected function extractOptions(Request $request): array
    {
        $headers = [
            'Content-Type' => 'text/xml',
        ];

        $body = $this->serializer->serialize($request, 'soap');

        return [
            'auth' => [$this->login, $this->password],
            'headers' => $headers,
            'body' => $body,
        ];
    }

    /**
     * @param Request $request
     * @param ResponseInterface $response
     * @return Response
     * @throws Exception
     */
    protected function deserialize(Request $request, ResponseInterface $response): Response
    {
        if (!$this->isXmlResponseBody($response)) {
            throw new ResponseException($response, $request);
        }

        $responseBody = (string)$response->getBody();

        return $this->serializer->deserialize($responseBody, $request->responseClassName(), 'soap');
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    protected function isXmlResponseBody(ResponseInterface $response): bool
    {
        $xml = (string)$response->getBody();
        $dom = new DOMDocument();
        return @$dom->loadXML($xml);
    }
}