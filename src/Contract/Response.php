<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Contract;

interface Response
{
    public function requestResult(): Result;
}