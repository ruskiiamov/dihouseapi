<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi;

use DMT\Soap\Serializer\SoapDateHandler;
use DMT\Soap\Serializer\SoapDeserializationVisitorFactory;
use DMT\Soap\Serializer\SoapMessageEventSubscriber;
use DMT\Soap\Serializer\SoapSerializationVisitorFactory;
use GuzzleHttp\Client as HttpClient;
use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\SerializerBuilder;
use LaptopDev\DihouseApi\Client\Catalog;
use LaptopDev\DihouseApi\Client\Order;

class DihouseApiClient
{
    /** @var Catalog */
    public $catalog;

    /** @var Order */
    public $order;

    /**
     * @param string $baseUri
     * @param string $login
     * @param string $password
     * @param int $timeout
     */
    public function __construct(
        string $baseUri,
        string $login,
        string $password,
        int $timeout = 10
    ) {
        $httpClient = new HttpClient([
            'base_uri' => $baseUri,
            'timeout' => $timeout
        ]);

        $serializer = SerializerBuilder::create()
            ->setSerializationVisitor('soap', new SoapSerializationVisitorFactory())
            ->setDeserializationVisitor('soap', new SoapDeserializationVisitorFactory())
            ->configureListeners(
                function (EventDispatcher $dispatcher) {
                    $dispatcher->addSubscriber(
                        new SoapMessageEventSubscriber()
                    );
                }
            )
            ->configureHandlers(
                function (HandlerRegistry $registry) {
                    $registry->registerSubscribingHandler(new SoapDateHandler());
                }
            )
            ->build();

        $this->catalog = new Catalog($httpClient, $serializer, $login, $password);
        $this->order = new Order($httpClient, $serializer, $login, $password);
    }
}