<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\OrderResponse;
use LaptopDev\DihouseApi\Common\Product;
use LaptopDev\DihouseApi\Common\ProductCategory;
use LaptopDev\DihouseApi\Common\ProductPrice;
use LaptopDev\DihouseApi\Common\ProductRemain;

class GetResponseResult extends SyncRequestResult
{
    /**
     * @JMS\SerializedName("ProductCategories")
     * @JMS\XmlList(entry = "ProductCategory")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ProductCategory>")
     *
     * @var ProductCategory[]|null
     */
    private $productCategories;

    /**
     * @JMS\SerializedName("Products")
     * @JMS\XmlList(entry = "Product")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\Product>")
     *
     * @var Product[]|null
     */
    private $products;

    /**
     * @JMS\SerializedName("ProductRemains")
     * @JMS\XmlList(entry = "ProductRemain")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ProductRemain>")
     *
     * @var ProductRemain[]|null
     */
    private $productRemains;

    /**
     * @JMS\SerializedName("ProductPricelist")
     * @JMS\XmlList(entry = "ProductPrice")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ProductPrice>")
     *
     * @var ProductPrice[]|null
     */
    private $productPricelist;

    /**
     * @JMS\SerializedName("OrderResponse")
     * @JMS\Type("LaptopDev\DihouseApi\Common\OrderResponse")
     *
     * @var OrderResponse|null
     */
    private $orderResponse;

    /**
     * @return ProductCategory[]|null
     */
    public function productCategories(): ?array
    {
        return $this->productCategories;
    }

    /**
     * @return Product[]|null
     */
    public function products(): ?array
    {
        return $this->products;
    }

    /**
     * @return ProductRemain[]|null
     */
    public function productRemains(): ?array
    {
        return $this->productRemains;
    }

    /**
     * @return ProductPrice[]|null
     */
    public function productPricelist(): ?array
    {
        return $this->productPricelist;
    }

    /**
     * @return OrderResponse|null
     */
    public function orderResponse(): ?OrderResponse
    {
        return $this->orderResponse;
    }
}