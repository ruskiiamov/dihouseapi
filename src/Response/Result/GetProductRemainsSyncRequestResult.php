<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\ProductRemain;

class GetProductRemainsSyncRequestResult extends SyncRequestResult
{
    /**
     * @JMS\SerializedName("ProductRemains")
     * @JMS\XmlList(entry = "ProductRemain")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ProductRemain>")
     *
     * @var ProductRemain[]|null
     */
    private $productRemains;

    /**
     * @return ProductRemain[]|null
     */
    public function productRemains(): ?array
    {
        return $this->productRemains;
    }
}