<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Result;

use LaptopDev\DihouseApi\Contract\Result;
use JMS\Serializer\Annotation as JMS;

class AsyncRequestResult implements Result
{
    /**
     * @JMS\SerializedName("RequestID")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $requestId;

    /**
     * @JMS\SerializedName("Result")
     * @JMS\Type("int")
     * @Required
     *
     * @var int
     */
    private $result;

    /**
     * @JMS\SerializedName("ErrorMessage")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @return string
     */
    public function requestId(): string
    {
        return $this->requestId;
    }

    /**
     * @return int
     */
    public function result(): int
    {
        return $this->result;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }
}