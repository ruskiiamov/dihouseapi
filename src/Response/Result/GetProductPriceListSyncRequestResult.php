<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\ProductPrice;

class GetProductPriceListSyncRequestResult extends SyncRequestResult
{
    /**
     * @JMS\SerializedName("ProductPricelist")
     * @JMS\XmlList(entry = "ProductPrice")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ProductPrice>")
     *
     * @var ProductPrice[]|null
     */
    private $productPricelist;

    /**
     * @return ProductPrice[]|null
     */
    public function productPricelist(): ?array
    {
        return $this->productPricelist;
    }
}