<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Contract\Result;

abstract class SyncRequestResult implements Result
{
    /**
     * @JMS\SerializedName("Result")
     * @JMS\Type("int")
     * @Required
     *
     * @var int
     */
    private $result;

    /**
     * @JMS\SerializedName("ErrorMessage")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $errorMessage;

    /**
     * @JMS\SerializedName("Method")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $method;

    /**
     * @return int
     */
    public function result(): int
    {
        return $this->result;
    }

    /**
     * @return string
     */
    public function errorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return string
     */
    public function method(): string
    {
        return $this->method;
    }
}