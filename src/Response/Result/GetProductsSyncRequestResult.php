<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\Product;

class GetProductsSyncRequestResult extends SyncRequestResult
{
    /**
     * @JMS\SerializedName("Products")
     * @JMS\XmlList(entry = "Product")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\Product>")
     *
     * @var Product[]|null
     */
    private $products;

    /**
     * @return Product[]|null
     */
    public function products(): ?array
    {
        return $this->products;
    }
}