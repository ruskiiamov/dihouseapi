<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Result;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Common\ProductCategory;

class GetProductCategoriesSyncRequestResult extends SyncRequestResult
{
    /**
     * @JMS\SerializedName("ProductCategories")
     * @JMS\XmlList(entry = "ProductCategory")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ProductCategory>")
     *
     * @var ProductCategory[]|null
     */
    private $productCategories;

    /**
     * @return ProductCategory[]|null
     */
    public function productCategories(): ?array
    {
        return $this->productCategories;
    }
}