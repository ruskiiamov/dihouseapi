<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Catalog\Sync;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Contract\Response;
use LaptopDev\DihouseApi\Contract\Result;
use LaptopDev\DihouseApi\Response\Result\GetProductRemainsSyncRequestResult;

class GetProductRemainsSyncResponse implements Response
{
    /**
     * @JMS\SerializedName("RequestResult")
     * @JMS\Type("LaptopDev\DihouseApi\Response\Result\GetProductRemainsSyncRequestResult")
     *
     * @var GetProductRemainsSyncRequestResult
     */
    private $requestResult;

    public function requestResult(): Result
    {
        return $this->requestResult;
    }
}