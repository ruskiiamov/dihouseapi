<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Response\Order\Async;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\DihouseApi\Contract\Response;
use LaptopDev\DihouseApi\Contract\Result;
use LaptopDev\DihouseApi\Response\Result\AsyncRequestResult;

class ChangeOrderAsyncResponse implements Response
{
    /**
     * @JMS\SerializedName("RequestResult")
     * @JMS\Type("LaptopDev\DihouseApi\Response\Result\AsyncRequestResult")
     *
     * @var AsyncRequestResult
     */
    private $requestResult;

    public function requestResult(): Result
    {
        return $this->requestResult;
    }
}