<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class ProductCategory
{
    /**
     * @JMS\SerializedName("Code")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $code;

    /**
     * @JMS\SerializedName("Name")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return$this->name;
    }
}