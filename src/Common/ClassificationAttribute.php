<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class ClassificationAttribute
{
    /**
     * @JMS\SerializedName("Name")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $name;

    /**
     * @JMS\SerializedName("Attributes")
     * @JMS\XmlList(entry = "Attribute")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\Attribute>")
     * @Required
     *
     * @var Attribute[]
     */
    private $attributes;

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return Attribute[]
     */
    public function attributes(): array
    {
        return $this->attributes;
    }
}