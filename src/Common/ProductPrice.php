<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class ProductPrice
{
    /**
     * @JMS\SerializedName("Code")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $code;

    /**
     * @JMS\SerializedName("Price")
     * @JMS\Type("float")
     * @Required
     *
     * @var float
     */
    private $price;

    /**
     * @JMS\SerializedName("RRC")
     * @JMS\Type("float")
     * @Required
     *
     * @var float
     */
    private $rrc;

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function rrc(): float
    {
        return $this->rrc;
    }
}