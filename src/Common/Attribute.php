<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class Attribute
{
    /**
     * @JMS\SerializedName("Name")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $name;

    /**
     * @JMS\SerializedName("Value")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $value;

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }
}