<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class ProductRemain
{
    /**
     * @JMS\SerializedName("Code")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $code;

    /**
     * @JMS\SerializedName("Remain")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $remain;

    /**
     * @JMS\SerializedName("Description")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $description;

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function remain(): int
    {
        return $this->remain;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }
}