<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use DateTime;
use JMS\Serializer\Annotation as JMS;

class OrderResponse
{
    /**
     * @JMS\SerializedName("OrderID")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $orderId;

    /**
     * @JMS\SerializedName("Date")
     * @JMS\Type("DateTime<'d.m.Y'>")
     * @Required
     *
     * @var DateTime
     */
    private $date;

    /**
     * @JMS\SerializedName("Number")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $number;

    /**
     * @JMS\SerializedName("Status")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $status;

    /**
     * @JMS\SerializedName("ReserveDate")
     * @JMS\Type("DateTime<'d.m.Y H:i:s'>")
     *
     * @var DateTime|null
     */
    private $reserveDate;

    /**
     * @JMS\SerializedName("Contragent")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $contragent;

    /**
     * @JMS\SerializedName("TotalCost")
     * @JMS\Type("float")
     * @Required
     *
     * @var float
     */
    private $totalCost;

    /**
     * @JMS\SerializedName("VAT")
     * @JMS\Type("float")
     * @Required
     *
     * @var float
     */
    private $vat;

    /**
     * @JMS\SerializedName("OrderEntries")
     * @JMS\XmlList(entry = "OrderEntry")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\OrderEntryResponse>")
     * @Required
     *
     * @var OrderEntryResponse[]
     */
    private $orderEntries;

    /**
     * @return string
     */
    public function orderId(): string
    {
        return $this->orderId;
    }

    /**
     * @return DateTime
     */
    public function date(): DateTime
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function number(): string
    {
        return $this->number;
    }

    /**
     * @return int
     */
    public function status(): int
    {
        return $this->status;
    }

    /**
     * @return DateTime|null
     */
    public function reserveDate(): ?DateTime
    {
        return $this->reserveDate;
    }

    /**
     * @return string
     */
    public function contragent(): string
    {
        return $this->contragent;
    }

    /**
     * @return float
     */
    public function totalCost(): float
    {
        return $this->totalCost;
    }

    /**
     * @return float
     */
    public function vat(): float
    {
        return $this->vat;
    }

    /**
     * @return OrderEntryResponse[]
     */
    public function orderEntries(): array
    {
        return $this->orderEntries;
    }
}