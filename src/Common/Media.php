<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class Media
{
    /**
     * @JMS\XmlList(inline = true, entry = "Image")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\Image>")
     * @Required
     *
     * @var Image[]
     */
    private $images;

    /**
     * @return Image[]
     */
    public function images(): array
    {
        return $this->images;
    }
}