<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class Image
{
    /**
     * @JMS\SerializedName("Height")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $height;

    /**
     * @JMS\SerializedName("Width")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $width;

    /**
     * @JMS\SerializedName("Type")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $type;

    /**
     * @JMS\SerializedName("URL")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $url;

    /**
     * @return string
     */
    public function height(): string
    {
        return $this->height;
    }

    /**
     * @return string
     */
    public function width(): string
    {
        return $this->width;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return $this->url;
    }
}