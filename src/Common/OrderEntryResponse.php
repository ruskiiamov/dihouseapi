<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class OrderEntryResponse
{
    /**
     * @JMS\SerializedName("ProductCode")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $productCode;

    /**
     * @JMS\SerializedName("Quantity")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $quantity;

    /**
     * @JMS\SerializedName("Unit")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $unit;

    /**
     * @JMS\SerializedName("VAT")
     * @JMS\Type("float")
     * @Required
     *
     * @var float
     */
    private $vat;

    /**
     * @JMS\SerializedName("TotalCost")
     * @JMS\Type("float")
     * @Required
     *
     * @var float
     */
    private $totalCost;

    /**
     * @return string
     */
    public function productCode(): string
    {
        return $this->productCode;
    }

    /**
     * @return int
     */
    public function quantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function unit(): int
    {
        return $this->unit;
    }

    /**
     * @return float
     */
    public function vat(): float
    {
        return $this->vat;
    }

    /**
     * @return float
     */
    public function totalCost(): float
    {
        return $this->totalCost;
    }
}