<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\XmlRoot("urn:OrderRef", namespace="urn:di-house.ru:CEI")
 */
class OrderRef
{
    /**
     * @JMS\SerializedName("urn:OrderID")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $orderId;

    /**
     * @return string
     */
    public function orderId(): string
    {
        return $this->orderId;
    }

    /**
     * @param string $orderId
     * @return $this
     */
    public function setOrderId(string $orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }
}