<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

class Product
{
    /**
     * @JMS\SerializedName("Code")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $code;

    /**
     * @JMS\SerializedName("Category")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $category;

    /**
     * @JMS\SerializedName("VendorCode")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $vendorCode;

    /**
     * @JMS\SerializedName("PartnerCode")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $partnerCode;

    /**
     * @JMS\SerializedName("Name")
     * @JMS\Type("string")
     * @Required
     *
     * @var string
     */
    private $name;

    /**
     * @JMS\SerializedName("Summary")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $summary;

    /**
     * @JMS\SerializedName("Description")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $description;

    /**
     * @JMS\SerializedName("Unit")
     * @JMS\Type("string")
     *
     * @var string
     */
    private $unit;

    /**
     * @JMS\SerializedName("ClassificationAttributes")
     * @JMS\XmlList(entry = "ClassificationAttribute")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\ClassificationAttribute>")
     * @Required
     *
     * @var ClassificationAttribute[]
     */
    private $classificationAttributes;

    /**
     * @JMS\SerializedName("Medias")
     * @JMS\XmlList(entry = "Media")
     * @JMS\Type("array<LaptopDev\DihouseApi\Common\Media>")
     * @Required
     *
     * @var Media[]
     */
    private $medias;

    /**
     * @JMS\SerializedName("RRC")
     * @JMS\Type("float")
     * @Required
     *
     * @var float
     */
    private $rrc;

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function category(): string
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function vendorCode(): string
    {
        return $this->vendorCode;
    }

    /**
     * @return string
     */
    public function partnerCode(): string
    {
        return $this->partnerCode;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function summary(): string
    {
        return $this->summary;
    }

    /**
     * @return string
     */
    public function description(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function unit(): string
    {
        return $this->unit;
    }

    /**
     * @return ClassificationAttribute[]
     */
    public function classificationAttributes(): array
    {
        return $this->classificationAttributes;
    }

    /**
     * @return Media[]
     */
    public function medias(): array
    {
        return $this->medias;
    }

    /**
     * @return float
     */
    public function rrc(): float
    {
        return $this->rrc;
    }
}