<?php

declare(strict_types=1);

namespace LaptopDev\DihouseApi\Common;

use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\XmlRoot("urn:OrderEntry", namespace="urn:di-house.ru:CEI")
 */
class OrderEntry
{
    /**
     * @JMS\SerializedName("urn:ProductCode")
     * @JMS\Type("string")
     * @JMS\XmlElement(cdata = false)
     * @Required
     *
     * @var string
     */
    private $productCode;

    /**
     * @JMS\SerializedName("urn:Quantity")
     * @JMS\Type("integer")
     * @Required
     *
     * @var int
     */
    private $quantity;

    /**
     * @return string
     */
    public function productCode(): string
    {
        return $this->productCode;
    }

    /**
     * @param string $productCode
     * @return $this
     */
    public function setProductCode(string $productCode): self
    {
        $this->productCode = $productCode;
        return $this;
    }

    /**
     * @return int
     */
    public function quantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }
}