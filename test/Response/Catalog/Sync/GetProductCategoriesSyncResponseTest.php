<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Catalog\Sync;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ProductCategory;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductCategoriesSyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductCategoriesSyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
                'Method' => 'GetProductCategoriesSync',
                'ProductCategories' => [
                    'ProductCategory1' => [],
                    'ProductCategory2' => [],
                ],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetProductCategoriesSyncResponse',
            $data
        );

        $getProductCategoriesSyncResponse = $this->serializer->deserialize(
            $serialized,
            GetProductCategoriesSyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getProductCategoriesSyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getProductCategoriesSyncResponse->requestResult()->errorMessage()
        );

        $this->assertEquals(
            $data['RequestResult']['Method'],
            $getProductCategoriesSyncResponse->requestResult()->method()
        );

        $this->assertCount(
            2,
            $getProductCategoriesSyncResponse->requestResult()->productCategories()
        );

        $this->assertInstanceOf(
            ProductCategory::class,
            $getProductCategoriesSyncResponse->requestResult()->productCategories()[0]
        );
    }
}