<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Catalog\Sync;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\Product;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductsSyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductsSyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
                'Method' => 'GetProductsSync',
                'Products' => [
                    'Product1' => [],
                    'Product2' => [],
                ],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetProductsSyncResponse',
            $data
        );

        $getProductsSyncResponse = $this->serializer->deserialize(
            $serialized,
            GetProductsSyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getProductsSyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getProductsSyncResponse->requestResult()->errorMessage()
        );

        $this->assertEquals(
            $data['RequestResult']['Method'],
            $getProductsSyncResponse->requestResult()->method()
        );

        $this->assertCount(
            2,
            $getProductsSyncResponse->requestResult()->products()
        );

        $this->assertInstanceOf(
            Product::class,
            $getProductsSyncResponse->requestResult()->products()[0]
        );
    }
}