<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Catalog\Sync;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ProductPrice;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductPriceListSyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductPriceListSyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
                'Method' => 'GetProductPriceListSync',
                'ProductPricelist' => [
                    'ProductPrice1' => [],
                    'ProductPrice2' => [],
                ],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetProductPriceListSyncResponse',
            $data
        );

        $getProductPriceListSyncResponse = $this->serializer->deserialize(
            $serialized,
            GetProductPriceListSyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getProductPriceListSyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getProductPriceListSyncResponse->requestResult()->errorMessage()
        );

        $this->assertEquals(
            $data['RequestResult']['Method'],
            $getProductPriceListSyncResponse->requestResult()->method()
        );

        $this->assertCount(
            2,
            $getProductPriceListSyncResponse->requestResult()->productPricelist()
        );

        $this->assertInstanceOf(
            ProductPrice::class,
            $getProductPriceListSyncResponse->requestResult()->productPricelist()[0]
        );
    }
}