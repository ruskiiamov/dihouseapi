<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Catalog\Sync;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ProductRemain;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductRemainsSyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductRemainsSyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
                'Method' => 'GetProductRemainsSync',
                'ProductRemains' => [
                    'ProductRemain1' => [],
                    'ProductRemain2' => [],
                ],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetProductRemainsSyncResponse',
            $data
        );

        $getProductRemainsSyncResponse = $this->serializer->deserialize(
            $serialized,
            GetProductRemainsSyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getProductRemainsSyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getProductRemainsSyncResponse->requestResult()->errorMessage()
        );

        $this->assertEquals(
            $data['RequestResult']['Method'],
            $getProductRemainsSyncResponse->requestResult()->method()
        );

        $this->assertCount(
            2,
            $getProductRemainsSyncResponse->requestResult()->productRemains()
        );

        $this->assertInstanceOf(
            ProductRemain::class,
            $getProductRemainsSyncResponse->requestResult()->productRemains()[0]
        );
    }
}