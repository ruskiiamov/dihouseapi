<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Catalog\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductCategoriesAsyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductCategoriesAsyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'RequestID' => 'test_request_id',
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetProductCategoriesAsyncResponse',
            $data
        );

        $getProductCategoriesAsyncResponse = $this->serializer->deserialize(
            $serialized,
            GetProductCategoriesAsyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['RequestID'],
            $getProductCategoriesAsyncResponse->requestResult()->requestId()
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getProductCategoriesAsyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getProductCategoriesAsyncResponse->requestResult()->errorMessage()
        );
    }
}