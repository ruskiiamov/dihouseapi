<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Catalog\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductRemainsAsyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductRemainsAsyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'RequestID' => 'test_request_id',
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetProductRemainsAsyncResponse',
            $data
        );

        $getProductRemainsAsyncResponse = $this->serializer->deserialize(
            $serialized,
            GetProductRemainsAsyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['RequestID'],
            $getProductRemainsAsyncResponse->requestResult()->requestId()
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getProductRemainsAsyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getProductRemainsAsyncResponse->requestResult()->errorMessage()
        );
    }
}