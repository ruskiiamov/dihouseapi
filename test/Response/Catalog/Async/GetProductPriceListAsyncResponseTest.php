<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Catalog\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductPriceListAsyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductPriceListAsyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'RequestID' => 'test_request_id',
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetProductPriceListAsyncResponse',
            $data
        );

        $getProductPriceListAsyncResponse = $this->serializer->deserialize(
            $serialized,
            GetProductPriceListAsyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['RequestID'],
            $getProductPriceListAsyncResponse->requestResult()->requestId()
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getProductPriceListAsyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getProductPriceListAsyncResponse->requestResult()->errorMessage()
        );
    }
}