<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Order\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Response\Order\Async\OrderInfoAsyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class OrderInfoAsyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'RequestID' => 'test_request_id',
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'OrderInfoAsyncResponse',
            $data
        );

        $orderInfoAsyncResponse = $this->serializer->deserialize(
            $serialized,
            OrderInfoAsyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['RequestID'],
            $orderInfoAsyncResponse->requestResult()->requestId()
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $orderInfoAsyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $orderInfoAsyncResponse->requestResult()->errorMessage()
        );
    }
}