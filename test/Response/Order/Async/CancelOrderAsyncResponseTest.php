<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Order\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Response\Order\Async\CancelOrderAsyncResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class CancelOrderAsyncResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'RequestID' => 'test_request_id',
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'CancelOrderAsyncResponse',
            $data
        );

        $cancelOrderAsyncResponse = $this->serializer->deserialize(
            $serialized,
            CancelOrderAsyncResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['RequestID'],
            $cancelOrderAsyncResponse->requestResult()->requestId()
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $cancelOrderAsyncResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $cancelOrderAsyncResponse->requestResult()->errorMessage()
        );
    }
}