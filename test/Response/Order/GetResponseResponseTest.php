<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Response\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\OrderResponse;
use LaptopDev\DihouseApi\Common\Product;
use LaptopDev\DihouseApi\Common\ProductCategory;
use LaptopDev\DihouseApi\Common\ProductPrice;
use LaptopDev\DihouseApi\Common\ProductRemain;
use LaptopDev\DihouseApi\Response\Order\GetResponseResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetResponseResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'RequestResult' => [
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
                'Method' => 'GetProductCategoriesSync',
                'ProductCategories' => [
                    'ProductCategory1' => [],
                    'ProductCategory2' => [],
                ],
                'OrderResponse' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetResponseResponse',
            $data
        );

        $getResponseResponse = $this->serializer->deserialize(
            $serialized,
            GetResponseResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getResponseResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getResponseResponse->requestResult()->errorMessage()
        );

        $this->assertEquals(
            $data['RequestResult']['Method'],
            $getResponseResponse->requestResult()->method()
        );

        $this->assertCount(
            2,
            $getResponseResponse->requestResult()->productCategories()
        );

        $this->assertInstanceOf(
            ProductCategory::class,
            $getResponseResponse->requestResult()->productCategories()[0]
        );

        $this->assertInstanceOf(
            OrderResponse::class,
            $getResponseResponse->requestResult()->orderResponse()
        );

        $data = [
            'RequestResult' => [
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
                'Method' => 'GetProductsSync',
                'Products' => [
                    'Product1' => [],
                    'Product2' => [],
                ],
                'OrderResponse' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetResponseResponse',
            $data
        );

        $getResponseResponse = $this->serializer->deserialize(
            $serialized,
            GetResponseResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getResponseResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getResponseResponse->requestResult()->errorMessage()
        );

        $this->assertEquals(
            $data['RequestResult']['Method'],
            $getResponseResponse->requestResult()->method()
        );

        $this->assertCount(
            2,
            $getResponseResponse->requestResult()->products()
        );

        $this->assertInstanceOf(
            Product::class,
            $getResponseResponse->requestResult()->products()[0]
        );

        $this->assertInstanceOf(
            OrderResponse::class,
            $getResponseResponse->requestResult()->orderResponse()
        );

        $data = [
            'RequestResult' => [
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
                'Method' => 'GetProductRemainsSync',
                'ProductRemains' => [
                    'ProductRemain1' => [],
                    'ProductRemain2' => [],
                ],
                'OrderResponse' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetResponseResponse',
            $data
        );

        $getResponseResponse = $this->serializer->deserialize(
            $serialized,
            GetResponseResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getResponseResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getResponseResponse->requestResult()->errorMessage()
        );

        $this->assertEquals(
            $data['RequestResult']['Method'],
            $getResponseResponse->requestResult()->method()
        );

        $this->assertCount(
            2,
            $getResponseResponse->requestResult()->productRemains()
        );

        $this->assertInstanceOf(
            ProductRemain::class,
            $getResponseResponse->requestResult()->productRemains()[0]
        );

        $this->assertInstanceOf(
            OrderResponse::class,
            $getResponseResponse->requestResult()->orderResponse()
        );

        $data = [
            'RequestResult' => [
                'Result' => 0,
                'ErrorMessage' => 'test_error_message',
                'Method' => 'GetProductPriceListSync',
                'ProductPricelist' => [
                    'ProductPrice1' => [],
                    'ProductPrice2' => [],
                ],
                'OrderResponse' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'GetResponseResponse',
            $data
        );

        $getResponseResponse = $this->serializer->deserialize(
            $serialized,
            GetResponseResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['RequestResult']['Result'],
            $getResponseResponse->requestResult()->result()
        );

        $this->assertEquals(
            $data['RequestResult']['ErrorMessage'],
            $getResponseResponse->requestResult()->errorMessage()
        );

        $this->assertEquals(
            $data['RequestResult']['Method'],
            $getResponseResponse->requestResult()->method()
        );

        $this->assertCount(
            2,
            $getResponseResponse->requestResult()->productPricelist()
        );

        $this->assertInstanceOf(
            ProductPrice::class,
            $getResponseResponse->requestResult()->productPricelist()[0]
        );

        $this->assertInstanceOf(
            OrderResponse::class,
            $getResponseResponse->requestResult()->orderResponse()
        );
    }
}