<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Traits;

use DMT\Soap\Serializer\SoapDateHandler;
use DMT\Soap\Serializer\SoapDeserializationVisitorFactory;
use DMT\Soap\Serializer\SoapMessageEventSubscriber;
use DMT\Soap\Serializer\SoapSerializationVisitorFactory;
use JMS\Serializer\EventDispatcher\EventDispatcher;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;

trait SoapSerializerBuilder
{
    /**
     * @return SerializerInterface
     */
    protected function buildSoapSerializer(): SerializerInterface
    {
        return SerializerBuilder::create()
            ->setSerializationVisitor('soap', new SoapSerializationVisitorFactory())
            ->setDeserializationVisitor('soap', new SoapDeserializationVisitorFactory())
            ->configureListeners(
                function (EventDispatcher $dispatcher) {
                    $dispatcher->addSubscriber(
                        new SoapMessageEventSubscriber()
                    );
                }
            )
            ->configureHandlers(
                function (HandlerRegistry $registry) {
                    $registry->registerSubscribingHandler(new SoapDateHandler());
                }
            )
            ->build();
    }
}