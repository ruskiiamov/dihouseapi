<?php

namespace LaptopDev\test\DihouseApi\Traits;

use DOMDocument;
use DOMElement;

trait ArrayToSoapMessageConverter
{
    /**
     * @param string $method
     * @param array $data
     * @param string $namespace
     * @param string $prefix
     * @return string
     */
    private function convertArrayToSoapMessage(string $root, array $data, string $namespace = '', string $prefix = ''): string
    {
        $soapMessageArray = [
            '<?xml version="1.0" encoding="UTF-8"?>',
            '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">',
            '<soap:Body/>',
            '</soap:Envelope>',
        ];

        $soapMessageString = implode('', $soapMessageArray);
        $dom = new DOMDocument();
        $dom->loadXML($soapMessageString);
        $bodyDOMNode = $dom->getElementsByTagName('Body')[0];
        if (empty($namespace) || empty($prefix)) {
            $methodDOMNode = $dom->createElement($root);
        } else {
            $methodDOMNode = $dom->createElementNS($namespace,$prefix . ':' . $root);
        }
        $bodyDOMNode->appendChild($methodDOMNode);

        $this->addDataToDomElement($methodDOMNode, $data, $namespace);

        return $dom->saveXML();
    }

    /**
     * @param DOMElement $domElement
     * @param array $data
     * @param string $namespace
     */
    private function addDataToDomElement(DOMElement $domElement, array $data, string $namespace = ''): void
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $key = preg_replace('/[0-9]+/', '', $key);
                $newElement = new DOMElement($key, '', $namespace);
                $domElement->appendChild($newElement);
                $this->addDataToDomElement($newElement, $value, $namespace);
            } else {
                $value = strval($value);
                $newElement = new DOMElement($key, $value, $namespace);
                $domElement->appendChild($newElement);
            }
        }
    }
}