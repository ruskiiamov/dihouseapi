<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Request\Order\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\OrderEntry;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\Order\Async\ChangeOrderAsyncRequest;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ChangeOrderAsyncRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'OrderRef' => [
                'OrderID' => 'test_id',
            ],
            'OrderEntriesType' => 1,
            'OrderEntries' => [
                'OrderEntry1' => [
                    'ProductCode' => 'test_code1',
                    'Quantity' => 10,
                ],
                'OrderEntry2' => [
                    'ProductCode' => 'test_code2',
                    'Quantity' => 20,
                ],
            ],
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'ChangeOrderAsync',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $orderRef = new OrderRef();
        $orderRef->setOrderId($data['OrderRef']['OrderID']);

        $orderEntry1 = new OrderEntry();
        $orderEntry1
            ->setProductCode($data['OrderEntries']['OrderEntry1']['ProductCode'])
            ->setQuantity($data['OrderEntries']['OrderEntry1']['Quantity']);

        $orderEntry2 = new OrderEntry();
        $orderEntry2
            ->setProductCode($data['OrderEntries']['OrderEntry2']['ProductCode'])
            ->setQuantity($data['OrderEntries']['OrderEntry2']['Quantity']);

        $request = new ChangeOrderAsyncRequest;
        $request
            ->setOrderRef($orderRef)
            ->setOrderEntriesType($data['OrderEntriesType'])
            ->setOrderEntries([$orderEntry1, $orderEntry2]);


        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}