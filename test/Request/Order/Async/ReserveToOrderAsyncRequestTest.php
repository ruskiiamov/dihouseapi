<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Request\Order\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ExtraInfoItem;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\Order\Async\ReserveToOrderAsyncRequest;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ReserveToOrderAsyncRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'OrderRef' => [
                'OrderID' => 'test_id',
            ],
            'ExtraInfo' => [
                'ExtraInfoItem1' => [
                    'Name' => 'imCustomerName',
                    'Value' => 'test_value',
                ],
                'ExtraInfoItem2' => [
                    'Name' => 'imAddress',
                    'Value' => 'test_value',
                ],
            ],
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'ReserveToOrderAsync',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $orderRef = new OrderRef();
        $orderRef->setOrderId($data['OrderRef']['OrderID']);

        $extraInfoItem1 = new ExtraInfoItem();
        $extraInfoItem1
            ->setName($data['ExtraInfo']['ExtraInfoItem1']['Name'])
            ->setValue($data['ExtraInfo']['ExtraInfoItem1']['Value']);

        $extraInfoItem2 = new ExtraInfoItem();
        $extraInfoItem2
            ->setName($data['ExtraInfo']['ExtraInfoItem2']['Name'])
            ->setValue($data['ExtraInfo']['ExtraInfoItem2']['Value']);

        $request = new ReserveToOrderAsyncRequest;
        $request
            ->setOrderRef($orderRef)
            ->setExtraInfo([$extraInfoItem1, $extraInfoItem2]);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}