<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Request\Order\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ExtraInfoItem;
use LaptopDev\DihouseApi\Common\OrderEntry;
use LaptopDev\DihouseApi\Request\Order\Async\NewOrderAsyncRequest;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class NewOrderAsyncRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'OrderType' => 4,
            'OrderNumber' => 'test_order_number',
            'OrderEntries' => [
                'OrderEntry1' => [
                    'ProductCode' => 'test_code1',
                    'Quantity' => 10,
                ],
                'OrderEntry2' => [
                    'ProductCode' => 'test_code2',
                    'Quantity' => 20,
                ],
            ],
            'ExtraInfo' => [
                'ExtraInfoItem1' => [
                    'Name' => 'imCustomerName',
                    'Value' => 'test_value',
                ],
                'ExtraInfoItem2' => [
                    'Name' => 'imAddress',
                    'Value' => 'test_value',
                ],
            ],
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'NewOrderAsync',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $orderEntry1 = new OrderEntry();
        $orderEntry1
            ->setProductCode($data['OrderEntries']['OrderEntry1']['ProductCode'])
            ->setQuantity($data['OrderEntries']['OrderEntry1']['Quantity']);

        $orderEntry2 = new OrderEntry();
        $orderEntry2
            ->setProductCode($data['OrderEntries']['OrderEntry2']['ProductCode'])
            ->setQuantity($data['OrderEntries']['OrderEntry2']['Quantity']);

        $extraInfoItem1 = new ExtraInfoItem();
        $extraInfoItem1
            ->setName($data['ExtraInfo']['ExtraInfoItem1']['Name'])
            ->setValue($data['ExtraInfo']['ExtraInfoItem1']['Value']);

        $extraInfoItem2 = new ExtraInfoItem();
        $extraInfoItem2
            ->setName($data['ExtraInfo']['ExtraInfoItem2']['Name'])
            ->setValue($data['ExtraInfo']['ExtraInfoItem2']['Value']);

        $request = new NewOrderAsyncRequest;
        $request
            ->setOrderType($data['OrderType'])
            ->setOrderNumber($data['OrderNumber'])
            ->setOrderEntries([$orderEntry1, $orderEntry2])
            ->setExtraInfo([$extraInfoItem1, $extraInfoItem2]);


        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}