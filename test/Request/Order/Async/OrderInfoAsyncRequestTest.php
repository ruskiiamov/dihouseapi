<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Request\Order\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\Order\Async\OrderInfoAsyncRequest;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class OrderInfoAsyncRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'OrderRef' => [
                'OrderID' => 'test_id',
            ],
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'OrderInfoAsync',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $orderRef = new OrderRef();
        $orderRef->setOrderId($data['OrderRef']['OrderID']);

        $request = new OrderInfoAsyncRequest;
        $request
            ->setOrderRef($orderRef);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}