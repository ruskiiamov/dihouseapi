<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Request\Order;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Request\Order\GetResponseRequest;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetResponseRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'RequestID' => 'test_request_id',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'GetResponse',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $request = new GetResponseRequest;
        $request
            ->setRequestId($data['RequestID']);

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}