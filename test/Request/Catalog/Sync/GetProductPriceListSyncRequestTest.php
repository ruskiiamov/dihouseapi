<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Request\Catalog\Sync;

use DateTime;
use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductPriceListSyncRequest;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductPriceListSyncRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'Code' => 'test_code',
            'Category' => 'test_category',
            'LastChangeDate' => '06.11.2021 15:05:20'
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'GetProductPriceListSync',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $request = new GetProductPriceListSyncRequest;
        $request
            ->setCode($data['Code'])
            ->setCategory($data['Category'])
            ->setLastChangeDate(
                new DateTime($data['LastChangeDate'])
            );

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}