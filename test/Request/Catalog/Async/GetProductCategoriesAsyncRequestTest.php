<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Request\Catalog\Async;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductCategoriesAsyncRequest;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class GetProductCategoriesAsyncRequestTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'GetProductCategoriesAsync',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $request = new GetProductCategoriesAsyncRequest;

        $serialized = $this->serializer->serialize($request, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}