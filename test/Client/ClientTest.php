<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Client;

use Exception;
use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Client\Client;
use LaptopDev\DihouseApi\Contract\Request;
use LaptopDev\DihouseApi\Contract\Response;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class ClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    /** @var Request */
    protected $request;

    /** @var ResponseInterface */
    protected $response;

    protected function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->login = 'test_login';
        $this->password = 'test_password';
        $this->request = Mockery::mock(Request::class);
        $this->response = Mockery::mock(ResponseInterface::class);
    }

    public function testSendRequest(): void
    {
        $this->httpClient
            ->shouldReceive('request')
            ->once()
            ->andReturn(
                Mockery::mock(ResponseInterface::class)
                ->shouldReceive('getBody')
                ->andReturn('<key>value</key>')
                ->getMock()
            );

        $this->serializer
            ->shouldReceive('serialize')
            ->once()
            ->andReturn('');

        $this->serializer
            ->shouldReceive('deserialize')
            ->once()
            ->andReturn(
                Mockery::mock(Response::class)
            );

        $client = new class(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedSendRequest(Request $request): Response
            {
                return $this->sendRequest($request);
            }
        };

        $this->request
            ->shouldReceive('responseClassName')
            ->once();

        $client->exposedSendRequest($this->request);
    }

    public function testExtractOptions(): void
    {
        $serialized = '
        <urn:OrderInfoAsync>
        <urn:OrderRef>
        <urn:OrderID>34348a0d-fba6-11e5-8c74-00155d0a6510</urn:OrderID>
        </urn:OrderRef>
        </urn:OrderInfoAsync>';

        $this->serializer
            ->shouldReceive('serialize')
            ->once()
            ->andReturn($serialized);

        $client = new class(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedExtractOptions(Request $request): array
            {
                return $this->extractOptions($request);
            }
        };

        $actualOptions = $client->exposedExtractOptions($this->request);
        $expectedOptions = [
            'auth' => [$this->login, $this->password],
            'headers' => [
                'Content-Type' => 'text/xml',
            ],
            'body' => $serialized,
        ];

        $this->assertEquals(
            $expectedOptions,
            $actualOptions
        );
    }

    public function testDeserialize(): void
    {
        $this->serializer
            ->shouldReceive('deserialize')
            ->once()
            ->andReturn(
                Mockery::mock(Response::class)
            );

        $client = new class(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedDeserialize(Request $request, ResponseInterface $response): Response
            {
                return $this->deserialize($request, $response);
            }
        };

        $this->request
            ->shouldReceive('responseClassName')
            ->once()
            ->andReturn(Response::class);

        $this->response
            ->shouldReceive('getBody')
            ->times(2)
            ->andReturn('<key>value</key>');

        $client->exposedDeserialize($this->request, $this->response);

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('text');

        $this->expectException(Exception::class);

        $client->exposedDeserialize($this->request, $this->response);
    }

    public function testIsXmlResponseBody(): void
    {
        $client = new class(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        ) extends Client {
            public function exposedIsXmlResponseBody(ResponseInterface $response): bool
            {
                return $this->isXmlResponseBody($response);
            }
        };

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('<key>value</key>');

        $trueResult = $client->exposedIsXmlResponseBody($this->response);

        $this->assertTrue($trueResult);

        $this->response
            ->shouldReceive('getBody')
            ->once()
            ->andReturn('text');

        $falseResult = $client->exposedIsXmlResponseBody($this->response);

        $this->assertFalse($falseResult);
    }
}