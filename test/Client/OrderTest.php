<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Client;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Client\Client;
use LaptopDev\DihouseApi\Client\Order;
use LaptopDev\DihouseApi\Request\Order\Async\CancelOrderAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\Async\ChangeOrderAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\Async\NewOrderAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\Async\OrderInfoAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\Async\ReserveToOrderAsyncRequest;
use LaptopDev\DihouseApi\Request\Order\GetResponseRequest;
use LaptopDev\DihouseApi\Response\Order\Async\CancelOrderAsyncResponse;
use LaptopDev\DihouseApi\Response\Order\Async\ChangeOrderAsyncResponse;
use LaptopDev\DihouseApi\Response\Order\Async\NewOrderAsyncResponse;
use LaptopDev\DihouseApi\Response\Order\Async\OrderInfoAsyncResponse;
use LaptopDev\DihouseApi\Response\Order\Async\ReserveToOrderAsyncResponse;
use LaptopDev\DihouseApi\Response\Order\GetResponseResponse;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

class OrderTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    protected function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->login = 'test_login';
        $this->password = 'test_password';
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendNewOrderAsyncRequest(): void
    {
        $request = Mockery::mock(NewOrderAsyncRequest::class);
        $response = Mockery::mock(NewOrderAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            NewOrderAsyncResponse::class,
            $catalog->sendNewOrderAsyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendReserveToOrderAsyncRequest(): void
    {
        $request = Mockery::mock(ReserveToOrderAsyncRequest::class);
        $response = Mockery::mock(ReserveToOrderAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            ReserveToOrderAsyncResponse::class,
            $catalog->sendReserveToOrderAsyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendCancelOrderAsyncRequest(): void
    {
        $request = Mockery::mock(CancelOrderAsyncRequest::class);
        $response = Mockery::mock(CancelOrderAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            CancelOrderAsyncResponse::class,
            $catalog->sendCancelOrderAsyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendChangeOrderAsyncRequest(): void
    {
        $request = Mockery::mock(ChangeOrderAsyncRequest::class);
        $response = Mockery::mock(ChangeOrderAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            ChangeOrderAsyncResponse::class,
            $catalog->sendChangeOrderAsyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendOrderInfoAsyncRequest(): void
    {
        $request = Mockery::mock(OrderInfoAsyncRequest::class);
        $response = Mockery::mock(OrderInfoAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            OrderInfoAsyncResponse::class,
            $catalog->sendOrderInfoAsyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetResponseRequest(): void
    {
        $request = Mockery::mock(GetResponseRequest::class);
        $response = Mockery::mock(GetResponseResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Order(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetResponseResponse::class,
            $catalog->sendGetResponseRequest($request)
        );
    }
}