<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Client;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Client\Catalog;
use LaptopDev\DihouseApi\Client\Client;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductCategoriesAsyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductPriceListAsyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductRemainsAsyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Async\GetProductsAsyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductCategoriesSyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductPriceListSyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductRemainsSyncRequest;
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductsSyncRequest;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductCategoriesAsyncResponse;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductPriceListAsyncResponse;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductRemainsAsyncResponse;
use LaptopDev\DihouseApi\Response\Catalog\Async\GetProductsAsyncResponse;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductCategoriesSyncResponse;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductPriceListSyncResponse;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductRemainsSyncResponse;
use LaptopDev\DihouseApi\Response\Catalog\Sync\GetProductsSyncResponse;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

class CatalogTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /** @var ClientInterface */
    protected $httpClient;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var string */
    protected $login;

    /** @var string */
    protected $password;

    protected function setUp(): void
    {
        $this->httpClient = Mockery::mock(ClientInterface::class);
        $this->serializer = Mockery::mock(SerializerInterface::class);
        $this->login = 'test_login';
        $this->password = 'test_password';
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetProductCategoriesSyncRequest(): void
    {
        $request = Mockery::mock(GetProductCategoriesSyncRequest::class);
        $response = Mockery::mock(GetProductCategoriesSyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetProductCategoriesSyncResponse::class,
            $catalog->sendGetProductCategoriesSyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetProductCategoriesAsyncRequest(): void
    {
        $request = Mockery::mock(GetProductCategoriesAsyncRequest::class);
        $response = Mockery::mock(GetProductCategoriesAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetProductCategoriesAsyncResponse::class,
            $catalog->sendGetProductCategoriesAsyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetProductsSyncRequest(): void
    {
        $request = Mockery::mock(GetProductsSyncRequest::class);
        $response = Mockery::mock(GetProductsSyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetProductsSyncResponse::class,
            $catalog->sendGetProductsSyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetProductsAsyncRequest(): void
    {
        $request = Mockery::mock(GetProductsAsyncRequest::class);
        $response = Mockery::mock(GetProductsAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetProductsAsyncResponse::class,
            $catalog->sendGetProductsAsyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetProductRemainsSyncRequest(): void
    {
        $request = Mockery::mock(GetProductRemainsSyncRequest::class);
        $response = Mockery::mock(GetProductRemainsSyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetProductRemainsSyncResponse::class,
            $catalog->sendGetProductRemainsSyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetProductRemainsAsyncRequest(): void
    {
        $request = Mockery::mock(GetProductRemainsAsyncRequest::class);
        $response = Mockery::mock(GetProductRemainsAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetProductRemainsAsyncResponse::class,
            $catalog->sendGetProductRemainsAsyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetProductPriceListSyncRequest(): void
    {
        $request = Mockery::mock(GetProductPriceListSyncRequest::class);
        $response = Mockery::mock(GetProductPriceListSyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetProductPriceListSyncResponse::class,
            $catalog->sendGetProductPriceListSyncRequest($request)
        );
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSendGetProductPriceListAsyncRequest(): void
    {
        $request = Mockery::mock(GetProductPriceListAsyncRequest::class);
        $response = Mockery::mock(GetProductPriceListAsyncResponse::class);

        $client = Mockery::mock('overload:' . Client::class);
        $client
            ->shouldReceive('sendRequest')
            ->once()
            ->andReturn($response);

        $catalog = new Catalog(
            $this->httpClient,
            $this->serializer,
            $this->login,
            $this->password
        );

        $this->assertInstanceOf(
            GetProductPriceListAsyncResponse::class,
            $catalog->sendGetProductPriceListAsyncRequest($request)
        );
    }
}