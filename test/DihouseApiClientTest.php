<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi;

use GuzzleHttp\Client as HttpClient;
use LaptopDev\DihouseApi\Client\Catalog;
use LaptopDev\DihouseApi\Client\Order;
use LaptopDev\DihouseApi\DihouseApiClient;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

class DihouseApiClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testConstruct(): void
    {
        Mockery::mock('overload:' . Catalog::class);
        Mockery::mock('overload:' . Order::class);

        $baseUri = 'test_uri';
        $timeout = 10;

        Mockery::mock('overload:' . HttpClient::class)
            ->shouldReceive('__construct')
            ->once()
            ->with([
                'base_uri' => $baseUri,
                'timeout' => $timeout
            ]);

        $dihouseApiClien = new DihouseApiClient(
            $baseUri,
            'tes_login',
            'test_password',
            $timeout
        );

        $this->assertInstanceOf(
            Catalog::class,
            $dihouseApiClien->catalog
        );

        $this->assertInstanceOf(
            Order::class,
            $dihouseApiClien->order
        );
    }
}