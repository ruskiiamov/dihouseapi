<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ProductRemain;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ProductRemainTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'Code' => 'test_code',
            'Remain' => 234,
            'Description' => 'test_description',
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'ProductRemain',
            $data
        );

        $productRemain = $this->serializer->deserialize(
            $serialized,
            ProductRemain::class,
            'soap'
        );

        $this->assertEquals(
            $data['Code'],
            $productRemain->code()
        );

        $this->assertEquals(
            $data['Remain'],
            $productRemain->remain()
        );

        $this->assertEquals(
            $data['Description'],
            $productRemain->description()
        );
    }
}