<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\Image;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ImageTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'Height' => 'test_height',
            'Width' => 'test_width',
            'Type' => 'test_type',
            'URL' => 'test_url',
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'Image',
            $data
        );

        $image = $this->serializer->deserialize(
            $serialized,
            Image::class,
            'soap'
        );

        $this->assertEquals(
            $data['Height'],
            $image->height()
        );

        $this->assertEquals(
            $data['Width'],
            $image->width()
        );

        $this->assertEquals(
            $data['Type'],
            $image->type()
        );

        $this->assertEquals(
            $data['URL'],
            $image->url()
        );
    }
}