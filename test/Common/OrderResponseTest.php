<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\OrderEntryResponse;
use LaptopDev\DihouseApi\Common\OrderResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class OrderResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'OrderID' => 'test_order_id',
            'Date' => '07.11.2021',
            'Number' => 'test_number',
            'Status' => 15,
            'ReserveDate' => '06.11.2021 21:05:23',
            'Contragent' => 'test_contragent',
            'TotalCost' => 765.4,
            'VAT' => 987.6,
            'OrderEntries' => [
                'OrderEntry1' => [],
                'OrderEntry2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'OrderResponse',
            $data
        );

        $orderResponse = $this->serializer->deserialize(
            $serialized,
            OrderResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['OrderID'],
            $orderResponse->orderId()
        );

        $this->assertEquals(
            $data['Date'],
            $orderResponse->date()->format('d.m.Y')
        );

        $this->assertEquals(
            $data['Number'],
            $orderResponse->number()
        );

        $this->assertEquals(
            $data['Status'],
            $orderResponse->status()
        );

        $this->assertEquals(
            $data['ReserveDate'],
            $orderResponse->reserveDate()->format('d.m.Y H:i:s')
        );

        $this->assertEquals(
            $data['Contragent'],
            $orderResponse->contragent()
        );

        $this->assertEquals(
            $data['TotalCost'],
            $orderResponse->totalCost()
        );

        $this->assertEquals(
            $data['VAT'],
            $orderResponse->vat()
        );

        $this->assertInstanceOf(
            OrderEntryResponse::class,
            $orderResponse->orderEntries()[0]
        );

        $this->assertCount(
            2,
            $orderResponse->orderEntries()
        );
    }
}