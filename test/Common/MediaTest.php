<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\Image;
use LaptopDev\DihouseApi\Common\Media;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class MediaTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'Image1' => [],
            'Image2' => [],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'Media',
            $data
        );

        $media = $this->serializer->deserialize(
            $serialized,
            Media::class,
            'soap'
        );

        $this->assertInstanceOf(
            Image::class,
            $media->images()[0]
        );

        $this->assertCount(
            2,
            $media->images()
        );
    }
}