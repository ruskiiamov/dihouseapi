<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\OrderEntry;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class OrderEntryTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'ProductCode' => 'test_product_code',
            'Quantity' => 123,
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'OrderEntry',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $orderEntry = new OrderEntry();
        $orderEntry
            ->setProductCode($data['ProductCode'])
            ->setQuantity($data['Quantity']);

        $serialized = $this->serializer->serialize($orderEntry, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}