<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\Attribute;
use LaptopDev\DihouseApi\Common\ClassificationAttribute;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ClassificationAttributeTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'Name' => 'test_name',
            'Attributes' => [
                'Attribute1' => [],
                'Attribute2' => [],
            ],
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'ClassificationAttribute',
            $data
        );

        $classificationAttribute = $this->serializer->deserialize(
            $serialized,
            ClassificationAttribute::class,
            'soap'
        );

        $this->assertEquals(
            $data['Name'],
            $classificationAttribute->name()
        );

        $this->assertCount(
            2,
            $classificationAttribute->attributes()
        );

        $this->assertInstanceOf(
            Attribute::class,
            $classificationAttribute->attributes()[0]
        );
    }
}