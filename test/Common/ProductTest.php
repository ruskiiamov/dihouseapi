<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ClassificationAttribute;
use LaptopDev\DihouseApi\Common\Media;
use LaptopDev\DihouseApi\Common\Product;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'Code' => 'test_code',
            'Category' => 'test_category',
            'VendorCode' => 'test_vendor_code',
            'PartnerCode' => 'test_partner_code',
            'Name' => 'test_name',
            'Summary' => 'test_summary',
            'Description' => 'test_description',
            'Unit' => 'test_unit',
            'ClassificationAttributes' => [
                'ClassificationAttribute1' => [],
                'ClassificationAttribute2' => [],
            ],
            'Medias' => [
                'Media1' => [],
                'Media2' => [],
            ],
            'RRC' => 456.7
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'Product',
            $data
        );

        $product = $this->serializer->deserialize(
            $serialized,
            Product::class,
            'soap'
        );

        $this->assertEquals(
            $data['Code'],
            $product->code()
        );

        $this->assertEquals(
            $data['Category'],
            $product->category()
        );

        $this->assertEquals(
            $data['VendorCode'],
            $product->vendorCode()
        );

        $this->assertEquals(
            $data['PartnerCode'],
            $product->partnerCode()
        );

        $this->assertEquals(
            $data['Name'],
            $product->name()
        );

        $this->assertEquals(
            $data['Summary'],
            $product->summary()
        );

        $this->assertEquals(
            $data['Description'],
            $product->description()
        );

        $this->assertEquals(
            $data['Unit'],
            $product->unit()
        );

        $this->assertInstanceOf(
            ClassificationAttribute::class,
            $product->classificationAttributes()[0]
        );

        $this->assertCount(
            2,
            $product->classificationAttributes()
        );

        $this->assertInstanceOf(
            Media::class,
            $product->medias()[0]
        );

        $this->assertCount(
            2,
            $product->medias()
        );

        $this->assertEquals(
            $data['RRC'],
            $product->rrc()
        );
    }
}