<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class OrderRefTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'OrderID' => 'test_order_id',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'OrderRef',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $orderRef = new OrderRef();
        $orderRef
            ->setOrderId($data['OrderID']);

        $serialized = $this->serializer->serialize($orderRef, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}