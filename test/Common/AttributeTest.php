<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\Attribute;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class AttributeTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'Name' => 'test_name',
            'Value' => 'test_value'
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'Attribute',
            $data
        );

        $attribute = $this->serializer->deserialize(
            $serialized,
            Attribute::class,
            'soap'
        );

        $this->assertEquals(
            $data['Name'],
            $attribute->name()
        );

        $this->assertEquals(
            $data['Value'],
            $attribute->value()
        );
    }
}