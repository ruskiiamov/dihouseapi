<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ExtraInfoItem;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ExtraInfoItemTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testSerialization(): void
    {
        $data = [
            'Name' => 'test_name',
            'Value' => 'test_value',
        ];

        $expectedSerialized = $this->convertArrayToSoapMessage(
            'ExtraInfoItem',
            $data,
            'urn:di-house.ru:CEI',
            'urn'
        );

        $extraInfoItem = new ExtraInfoItem();
        $extraInfoItem
            ->setName($data['Name'])
            ->setValue($data['Value']);

        $serialized = $this->serializer->serialize($extraInfoItem, 'soap');

        $this->assertXmlStringEqualsXmlString(
            $expectedSerialized,
            $serialized
        );
    }
}