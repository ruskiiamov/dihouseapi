<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ProductCategory;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ProductCategoryTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'Code' => 'test_code',
            'Name' => 'test_name',
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'ProductCategory',
            $data
        );

        $productCategory = $this->serializer->deserialize(
            $serialized,
            ProductCategory::class,
            'soap'
        );

        $this->assertEquals(
            $data['Code'],
            $productCategory->code()
        );

        $this->assertEquals(
            $data['Name'],
            $productCategory->name()
        );
    }
}