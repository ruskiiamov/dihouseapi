<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\ProductPrice;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class ProductPriceTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'Code' => 'test_code',
            'Price' => 123.4,
            'RRC' => 567.8
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'ProductPrice',
            $data
        );

        $productPrice = $this->serializer->deserialize(
            $serialized,
            ProductPrice::class,
            'soap'
        );

        $this->assertEquals(
            $data['Code'],
            $productPrice->code()
        );

        $this->assertEquals(
            $data['Price'],
            $productPrice->price()
        );

        $this->assertEquals(
            $data['RRC'],
            $productPrice->rrc()
        );
    }
}