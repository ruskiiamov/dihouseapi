<?php

declare(strict_types=1);

namespace LaptopDev\test\DihouseApi\Common;

use JMS\Serializer\SerializerInterface;
use LaptopDev\DihouseApi\Common\OrderEntryResponse;
use LaptopDev\test\DihouseApi\Traits\ArrayToSoapMessageConverter;
use LaptopDev\test\DihouseApi\Traits\SoapSerializerBuilder;
use PHPUnit\Framework\TestCase;

class OrderEntryResponseTest extends TestCase
{
    use SoapSerializerBuilder;
    use ArrayToSoapMessageConverter;

    /** @var SerializerInterface */
    private $serializer;

    protected function setUp(): void
    {
        $this->serializer = $this->buildSoapSerializer();
    }

    public function testDeserialization(): void
    {
        $data = [
            'ProductCode' => 'test_product_code',
            'Quantity' => 123,
            'Unit' => 876,
            'VAT' => 234.5,
            'TotalCost' => 345.6
        ];

        $serialized = $this->convertArrayToSoapMessage(
            'OrderEntryResponse',
            $data
        );

        $orderEntryResponse = $this->serializer->deserialize(
            $serialized,
            OrderEntryResponse::class,
            'soap'
        );

        $this->assertEquals(
            $data['ProductCode'],
            $orderEntryResponse->productCode()
        );

        $this->assertEquals(
            $data['Quantity'],
            $orderEntryResponse->quantity()
        );

        $this->assertEquals(
            $data['Unit'],
            $orderEntryResponse->unit()
        );

        $this->assertEquals(
            $data['VAT'],
            $orderEntryResponse->vat()
        );

        $this->assertEquals(
            $data['TotalCost'],
            $orderEntryResponse->totalCost()
        );
    }
}