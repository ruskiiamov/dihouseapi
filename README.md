# dihouseApi


## КЛИЕНТ
Необходимо указать адрес сервиса, логин и пароль пользователя сайта di-house.ru
#### Пример:
```php
use LaptopDev\DihouseApi\DihouseApiClient;

$client = new DihouseApiClient(
    'https://di-house.ru/dihousewebservices/unvrsl',
    'your_login',
    'your_password'
);
```

## API КАТАЛОГА
Интерфейс для работы с продуктовым каталогом

Предусмотрены два типа методов: синхронные и асинхронные. Синхронные
методы позволяют в ответном сообщении получить результат. Асинхронные методы
позволяют не ждать ответа, а запросить результат обработки позже.

### Методы получения списка категорий
- GetProductCategoriesSync
- GetProductCategoriesAsync
- Методы не имеют входящих параметров
##### Пример выполнения запроса:
```php
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductCategoriesSyncRequest;

$request = new GetProductCategoriesSyncRequest();

$response = $client->catalog->sendGetProductCategoriesSyncRequest($request);
```
### Методы получение описания, остатков или цен продуктов
- GetProductsSync
- GetProductsAsync
- GetProductRemainsSync
- GetProductRemainsAsync
- GetProductPriceListSync
- GetProductPriceListAsync
- На вход методов можно подать код продукта, код категории или дату изменения 
продукта. Хотя бы один из входных параметров должен быть обязательно заполнен
##### Пример выполнения запроса:
```php
use LaptopDev\DihouseApi\Request\Catalog\Sync\GetProductsSyncRequest;

$request = new GetProductsSyncRequest();
$request
    ->setCode('00-00022875')
    ->setCategory('8796322431118')
    ->setLastChangeDate(new DateTime('2016-02-29 00:00:00'));
    
$response = $client->catalog->sendGetProductsSyncRequest($request);
```

## API ЗАКАЗОВ

Интерйес для работы с заказами

Предусмотрены только асинхронные методы.

### Создание нового заказа
- NewOrderAsync
- Входные параметры для запроса: тип заказа (4 – резерв, 7 – заказ), номер заказа по данным
клиента, список позиций заказа и дополнительная информация по заказу
##### Пример выполнения запроса:
```php
use LaptopDev\DihouseApi\Common\ExtraInfoItem;
use LaptopDev\DihouseApi\Common\OrderEntry;
use LaptopDev\DihouseApi\Request\Order\Async\NewOrderAsyncRequest;

$orderEntry = new OrderEntry();
$orderEntry
    ->setProductCode('your_product_code')
    ->setQuantity(100);

$extraInfoItem = new ExtraInfoItem();
$extraInfoItem
    ->setName('imCustomerName')
    ->setValue('your_customer_name');

$request = new NewOrderAsyncRequest();
$request
    ->setOrderType(7)
    ->setOrderNumber('your_order_number')
    ->addOrderEntry($orderEntry)
    ->addExtraInfoItem($extraInfoItem);

$response = $client->order->sendNewOrderAsyncRequest($request);
```

### Изменение типа заказа с "Резерв" на "Заказ"
- ReserveToOrderAsync
- Входные параметры запроса: идентификационные данные заказа и дополнительная 
информация по заказу
##### Пример выполнения запроса:
```php
use LaptopDev\DihouseApi\Common\ExtraInfoItem;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\Order\Async\ReserveToOrderAsyncRequest;

$orderRef = new OrderRef();
$orderRef
    ->setOrderId('your_order_id');

$extraInfoItem = new ExtraInfoItem();
$extraInfoItem
    ->setName('imPhone')
    ->setValue('your_phone');

$request = new ReserveToOrderAsyncRequest();
$request
    ->setOrderRef($orderRef)
    ->addExtraInfoItem($extraInfoItem);

$response = $client->order->sendReserveToOrderAsyncRequest($request);
```

### Отмена заказа
- CancelOrderAsync
- Входной параметр: идентификационные данные заказа
```php
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\Order\Async\CancelOrderAsyncRequest;

$orderRef = new OrderRef();
$orderRef
    ->setOrderId('your_order_id');

$request = new CancelOrderAsyncRequest();
$request
    ->setOrderRef($orderRef);

$response = $client->order->sendCancelOrderAsyncRequest($request);
```

### Изменение заказа
- ChangeOrderAsync
- Входные параметры: идентификационные данные заказа, тип обработки заказа (1 – 
передается актуальное значение всех строк заказа, 2 – передается актуальное 
значение измененных строк заказа), список позиций заказа
```php
use LaptopDev\DihouseApi\Common\OrderEntry;
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\Order\Async\ChangeOrderAsyncRequest;

$orderRef = new OrderRef();
$orderRef
    ->setOrderId('your_order_id');
    
$orderEntry = new OrderEntry();
$orderEntry
    ->setProductCode('your_product_code')
    ->setQuantity(100);
    
$request = new ChangeOrderAsyncRequest();
$request
    ->setOrderRef($orderRef)
    ->setOrderEntriesType(1)
    ->addOrderEntry($orderEntry);
    
$response = $client->order->sendChangeOrderAsyncRequest($request);
```

### Получение информации о заказе
- OrderInfoAsync
- Входной параметр: идентификационные данные заказа
```php
use LaptopDev\DihouseApi\Common\OrderRef;
use LaptopDev\DihouseApi\Request\Order\Async\OrderInfoAsyncRequest;

$orderRef = new OrderRef();
$orderRef
    ->setOrderId('your_order_id');

$request = new OrderInfoAsyncRequest();
$request
    ->setOrderRef($orderRef);

$response = $client->order->sendOrderInfoAsyncRequest($request);
```

## Получение результата обработки запросов, вызванных в асинхронном режиме
- GetResponse
- Входной параметр: идентификатор запроса
```php
use LaptopDev\DihouseApi\Request\Order\GetResponseRequest;

$request = new GetResponseRequest();
$request
    ->setRequestId('you_request_id');

$response = $client->order->sendGetResponseRequest($request);
```